import os.path
from resource_management.core.resources.system import File, Execute

# pylint: disable=unused-argument
class Cassandra(object):

    def configure(self, env):
        import params
        import yaml

        File(os.path.join(params.cassandra_conf_dir, 'cassandra.yaml'),
             content=yaml.safe_dump(params.cassandra_configs),
             mode=0644,
             owner=params.cassandra_user)

    def start(self, env):
        Execute(('systemctl', 'start', 'cassandra'))

    def stop(self, env):
        Execute(('systemctl', 'stop', 'cassandra'))

    def status(self, env):
        Execute(('systemctl', 'status', 'cassandra'))
